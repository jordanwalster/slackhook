# Project Title:    Slackhook v1.2
# Date:             17/07/2018
# Author:           Jordan Walster

import urllib.request
import json


def main_menu():
    main_menu_input = input("\nReturn to main menu? [Y]/[N]").lower()
    if main_menu_input == "y":
        from slackhook import main
        main()
    else:
        sendmsg()


def sendmsg():

    message = input("\nEnter Message: ")

    slack_data = \
        {
            "text": message
        }

    webhook_url = 'https://hooks.slack.com/services/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'
    req = urllib.request.Request(webhook_url)

    req.add_header('Content-Type', 'application/json; charset=utf-8')
    json_data = json.dumps(slack_data)

    json_data_encoded = json_data.encode('utf-8')

    req.add_header('Content-Length', len(json_data_encoded))

    # print(json_data_encoded)

    urllib.request.urlopen(req, json_data_encoded)

    main_menu()


sendmsg()
