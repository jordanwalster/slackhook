# Project Title:    Slackhook v1.2
# Date:             17/07/2018
# Author:           Jordan Walster

import datetime
import time


def main():

    # Select function

    print("""
    
    Today's date is """ + datetime.datetime.today().strftime('%d/%m/%Y') + """
    
    Press [S] to send a message to Slack as Slackhook.
    
    Press [C] to test a connection to a website.
    
        
    """)

    sel()


def sel():
    selection = input("Enter starting function: ").lower()

    if selection == "s":
        from sendmsg import sendmsg
        sendmsg()
    if selection == "c":
        from connection import conn
        conn()
    else:
        print("Invalid Entry, please refer to the input codes above.")
        time.sleep(2)
        main()


main()