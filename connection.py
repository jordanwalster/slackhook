# Project Title:    Slackhook v1.2
# Date:             17/07/2018
# Author:           Jordan Walster

import urllib.request
import json
import os


def main_menu():
    main_menu_input = input("\nReturn to main menu? [Y]/[N] ").lower()
    if main_menu_input == "y":
        from slackhook import main
        main()
    else:
        conn()


def conn():
    global ping_error, hostname
    hostname = input("\nEnter hostname: ")
    response = os.system("ping " + hostname)

    print(response)

    if response == 0:
        ping_error = 0

    else:
        ping_error = 1

    get_page("http://" + hostname)


def get_page(hostname):
    global fetch_error
    req = urllib.request.Request(hostname)
    try:
        urllib.request.urlopen(req)
        fetch_error = 0
    except urllib.request.URLError:
        fetch_error = 1
    error_res()


def error_res():
    if ping_error + fetch_error > 0:

        if ping_error > 0:
            error_message = "Failed to ping host"
        elif fetch_error > 0:
            error_message = "Failed to fetch page from host"

        slack_data = \
            {
                "attachments": [
                    {
                        "color": "#DB0000",
                        "fields": [
                            {
                                "title": "Error!",
                                "value": "An error has occurred. " + error_message,
                            }
                        ]
                    }
                ]
            }

        webhook_url = 'https://hooks.slack.com/services/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'
        req = urllib.request.Request(webhook_url)

        req.add_header('Content-Type', 'application/json; charset=utf-8')
        json_data = json.dumps(slack_data)

        json_data_encoded = json_data.encode('utf-8')

        req.add_header('Content-Length', len(json_data_encoded))

        # print(json_data_encoded)

        urllib.request.urlopen(req, json_data_encoded)

        main_menu()



    else:
        slack_data = \
            {
                "attachments": [
                    {
                        "color": "#228B22",
                        "fields": [
                            {
                                "title": "Connection Established",
                                "value": "The server has established a connection to " + hostname,
                            }
                        ]
                    }
                ]
            }

        webhook_url = 'https://hooks.slack.com/services/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'
        req = urllib.request.Request(webhook_url)

        req.add_header('Content-Type', 'application/json; charset=utf-8')
        json_data = json.dumps(slack_data)

        json_data_encoded = json_data.encode('utf-8')

        req.add_header('Content-Length', len(json_data_encoded))

       # print(json_data_encoded)

        urllib.request.urlopen(req, json_data_encoded)

        main_menu()


conn()
